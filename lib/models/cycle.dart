class Cycle {
  final int sets;
  final int work;
  final int rest;
  final String title;
  final int prepare;
  final int cooldown;

  const Cycle({
      this.sets,
      this.work,
      this.rest,
      this.title,
      this.prepare,
      this.cooldown
  });
}
