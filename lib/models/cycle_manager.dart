import 'dart:core';
import 'package:interval_timer/models/cycle.dart';

abstract class CycleManager {
  static List<Cycle> _cycleList = [];

  static addCycle(Cycle cycle){
    _cycleList.insert(0, cycle);
  }

  static getCycleList(){
    return _cycleList;
  }

  static int cycleListSize(){
    return _cycleList.length;
  }
}
