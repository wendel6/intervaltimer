import 'package:flutter/material.dart';
import 'dart:async';

import '../widgets/main_page/lower_part.dart';
import '../widgets/main_page/main_card.dart';
import '../widgets/navbar.dart';
import 'package:volume_control/volume_control.dart';
import 'package:just_audio/just_audio.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  @override
  void initState(){
    super.initState();
    initVolumeState();
  }

  //init volume_control plugin
  Future<void> initVolumeState() async {
    if (!mounted) return;

    //read the current volume
    _val = await VolumeControl.volume;
    setState(() {
    });
  }

  double _val = 0.5;
  Timer timer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.blueGrey,
        title: NavBar(),
        centerTitle: true,
      ),
      body: Container(
        height: double.maxFinite,
        color: Colors.grey[300],
        child: SingleChildScrollView(
          child: Column(
            children: [
              MainCard(),
              LowerPart(),
            ],
          ),
        ),
      ),
    );
  }
}
