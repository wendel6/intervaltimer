import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:interval_timer/models/card_name.dart';
import 'package:interval_timer/models/cycle.dart';
import 'package:interval_timer/models/cycle_manager.dart';
import 'package:interval_timer/models/main_card_cycle_values.dart';
import 'package:interval_timer/models/secondary_card_cycle_values.dart';
import 'package:interval_timer/widgets/main_page/main_card_widgets/timer_category.dart';

class EditPage extends StatefulWidget {
  final Cycle cycle;
  final int index;

  const EditPage({Key key, this.cycle, this.index}) : super(key: key);

  @override
  _EditPageState createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  final titleController = TextEditingController();
  Cycle newCycle;

  void fillInputData() {
    if(widget.index != null){
      titleController.text = CycleManager.getCycleList()[widget.index].title;
      MainCardCycleValues.sets = CycleManager.getCycleList()[widget.index].sets;
      MainCardCycleValues.work = CycleManager.getCycleList()[widget.index].work;
      MainCardCycleValues.rest = CycleManager.getCycleList()[widget.index].rest;
      SecCycleValues.prepare =
          CycleManager.getCycleList()[widget.index].prepare ?? 0;
      SecCycleValues.cooldown =
          CycleManager.getCycleList()[widget.index].cooldown ?? 0;
    }
  }

  void finalizeAndSave() {
    CardName.title = null;
    SecCycleValues.prepare = 0;
    SecCycleValues.cooldown = 0;

    // Find an alternative to refresh mains screen
    Navigator.pushNamed(context, '/');
  }

  @override
  Widget build(BuildContext context) {
    fillInputData();

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            SizedBox(
              width: 250,
            ),
            IconButton(
              icon: Icon(
                Icons.save,
              ),
              onPressed: () => {
                if (widget.index != null)
                  {
                    newCycle = Cycle(
                      sets: MainCardCycleValues.sets,
                      rest: MainCardCycleValues.rest,
                      work: MainCardCycleValues.work,
                      prepare: SecCycleValues.prepare,
                      cooldown: SecCycleValues.cooldown,
                      title: titleController.text == ""
                          ? formatDate(
                              new DateTime.now(),
                              [
                                'Saved-',
                                yyyy,
                                '-',
                                mm,
                                '-',
                                dd,
                                ' ',
                                HH,
                                ':',
                                nn,
                                ':',
                                ss
                              ],
                            )
                          : titleController.text,
                    ),
                    if(CycleManager.cycleListSize() != 0 ){
                      CycleManager.getCycleList()[widget.index] = newCycle,
                    }else{
                      CycleManager.getCycleList().add(newCycle),
                    },
                    finalizeAndSave()
                  }
                else
                  {
                    CycleManager.addCycle(
                      Cycle(
                        sets: MainCardCycleValues.sets,
                        rest: MainCardCycleValues.rest,
                        work: MainCardCycleValues.work,
                        prepare: SecCycleValues.prepare,
                        cooldown: SecCycleValues.cooldown,
                        title: titleController.text == ""
                            ? formatDate(
                                new DateTime.now(),
                                [
                                  'Saved-',
                                  yyyy,
                                  '-',
                                  mm,
                                  '-',
                                  dd,
                                  ' ',
                                  HH,
                                  ':',
                                  nn,
                                  ':',
                                  ss
                                ],
                              )
                            : titleController.text,
                      ),
                    ),
                  },
                finalizeAndSave(),
              },
            ),
          ],
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.grey[300],
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: titleController,
                  cursorColor: Colors.blueGrey,
                  decoration: InputDecoration(
                    labelText: "Preset Name",
                    labelStyle: TextStyle(color: Colors.grey),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.amber),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              TimerCategory(title: "Prepare"),
              TimerCategory(title: "Sets"),
              TimerCategory(title: "Work"),
              TimerCategory(title: "Rest"),
              TimerCategory(title: "Cooldown"),
            ],
          ),
        ),
      ),
    );
  }
}
