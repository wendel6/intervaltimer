import 'package:flutter/material.dart';
import 'package:interval_timer/pages/timer/prepare_page.dart';
import 'package:interval_timer/widgets/cycle_exit_button.dart';
import 'package:timer_count_down/timer_controller.dart';
import 'package:timer_count_down/timer_count_down.dart';

import '../../models/cycle.dart';
import '../../models/main_card_cycle_values.dart';
import '../../widgets/navbar.dart';
import '../main_page.dart';

class FinishedPage extends StatefulWidget {
  final Cycle cycle;

  FinishedPage({Key key, this.cycle}) : super(key: key);

  @override
  _FinishedPageState createState() => _FinishedPageState();
}

class _FinishedPageState extends State<FinishedPage> {
  final CountdownController controller = CountdownController();

  bool _isPause = false;
  IconData buttonIcon = Icons.pause;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Container(
          color: Colors.cyan,
          child: Column(
            children: [
              if (_isPause == true)
                CycleExitButton(controller),
              if (_isPause == false)
                SizedBox(
                  height: 140,
                ),
              Container(
                child: Center(
                  child: Text(
                    "FINISHED",
                    style: TextStyle(
                        fontSize: 75,
                        fontWeight: FontWeight.bold,
                        color: Colors.cyan[200]),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 350.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      style: ButtonStyle(
                        shadowColor:
                          MaterialStateProperty.all<Color>(Colors.black),
                        elevation:
                          MaterialStateProperty.all<double>(5),
                        backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.grey[700]),
                        overlayColor:
                          MaterialStateProperty.all<Color>(Colors.grey[800]),
                      ),
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MainPage(),
                        ),
                      ),
                      child: Container(
                        height: 75,
                        width: 75,
                        child: Icon(
                          Icons.stop,
                          size: 50,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    TextButton(
                      style: ButtonStyle(
                        shadowColor:
                          MaterialStateProperty.all<Color>(Colors.black),
                        elevation:
                          MaterialStateProperty.all<double>(5),
                        backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.grey[700]),
                        overlayColor:
                          MaterialStateProperty.all<Color>(Colors.grey[800]),
                      ),
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => PreparePage(
                            cycle: widget.cycle,
                          ),
                        ),
                      ),
                      child: Container(
                        height: 75,
                        width: 75,
                        child: Icon(
                          Icons.autorenew,
                          size: 50,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
