import 'package:flutter/material.dart';
import 'package:interval_timer/pages/timer/work_page.dart';
import 'package:interval_timer/widgets/cycle_exit_button.dart';
import 'package:timer_count_down/timer_controller.dart';
import 'package:timer_count_down/timer_count_down.dart';
import 'package:tinycolor/tinycolor.dart';
import 'package:flutter_beep/flutter_beep.dart';

import '../../models/cycle.dart';
import '../../models/main_card_cycle_values.dart';
import '../../widgets/navbar.dart';

class RestPage extends StatefulWidget {
  final Cycle cycle;
  final int workCycles;

  RestPage({Key key, this.cycle, this.workCycles}) : super(key: key);

  @override
  _RestPageState createState() => _RestPageState();
}

class _RestPageState extends State<RestPage> {
  final CountdownController controller = CountdownController();

  bool _isPause = false;
  IconData buttonIcon = Icons.pause;
  TinyColor backgroundColor = TinyColor(Colors.blue);

  format(Duration d) => d.toString().split('.')[0].padLeft(8, "0").substring(3);

  @override
  void initState() {
    FlutterBeep.playSysSound(AndroidSoundIDs.TONE_CDMA_ABBR_ALERT);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          automaticallyImplyLeading: false,
          title: NavBar(),
          centerTitle: true,
        ),
        body: Container(
          color: backgroundColor.color,
          child: Column(
            children: [
              if (_isPause == true) CycleExitButton(controller),
              if (_isPause == false)
                SizedBox(
                  height: 71,
                ),
              Container(
                child: Center(
                  child: Text(
                    widget.workCycles.toString(),
                    style: TextStyle(
                      fontSize: 100,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Container(
                child: Center(
                  child: Countdown(
                    controller: controller,
                    seconds: widget.cycle.rest,
                    build: (BuildContext context, double time) {
                      if (time <= 3) {
                        FlutterBeep.playSysSound(
                            AndroidSoundIDs.TONE_CDMA_ABBR_ALERT);
                      };

                      return Text(
                        format(Duration(seconds: time.toInt())),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 150,
                        ),
                      );
                    },
                    interval: Duration(milliseconds: 1000),
                    onFinished: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => WorkPage(
                            cycle: widget.cycle,
                            workCycles: widget.workCycles - 1,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              Container(
                child: Center(
                  child: Text(
                    "REST",
                    style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color: Colors.green[200]),
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(buttonIcon),
          onPressed: () {
            final isCompleted = controller.isCompleted;
            isCompleted ? print("The End") : print("Still Not Over");

            if (_isPause == false) {
              print("Pause");

              setState(() {
                _isPause = true;
                controller.pause();
                buttonIcon = Icons.play_arrow;
                backgroundColor = TinyColor(Colors.blue).darken(20);
              });
            } else {
              print("Playing");

              setState(() {
                _isPause = false;
                controller.resume();
                buttonIcon = Icons.pause;
                backgroundColor = TinyColor(Colors.blue);
              });
            }
          },
        ),
      ),
    );
  }
}
