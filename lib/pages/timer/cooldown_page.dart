import 'package:flutter/material.dart';
import 'package:interval_timer/pages/timer/finished_page.dart';
import 'package:interval_timer/pages/timer/work_page.dart';
import 'package:interval_timer/widgets/cycle_exit_button.dart';
import 'package:timer_count_down/timer_controller.dart';
import 'package:timer_count_down/timer_count_down.dart';
import 'package:tinycolor/tinycolor.dart';

import '../../models/cycle.dart';
import '../../models/main_card_cycle_values.dart';
import '../../widgets/navbar.dart';

class CoolDownPage extends StatefulWidget {
  final Cycle cycle;

  CoolDownPage({Key key, this.cycle}) : super(key: key);

  @override
  _CoolDownPageState createState() => _CoolDownPageState();
}

class _CoolDownPageState extends State<CoolDownPage> {
  final CountdownController controller = CountdownController();

  bool _isPause = false;
  IconData buttonIcon = Icons.pause;
  TinyColor backgroundColor = TinyColor(Colors.orange);
  format(Duration d) => d.toString().split('.')[0].padLeft(8, "0").substring(3);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          automaticallyImplyLeading: false,
          title: NavBar(),
          centerTitle: true,
        ),
        body: Container(
          color: backgroundColor.color,
          child: Column(
            children: [
              if (_isPause == true)
                CycleExitButton(controller),
              if (_isPause == false)
                SizedBox(
                  height: 71,
                ),
              Container(
                child: Center(
                  child: Countdown(
                    controller: controller,
                    seconds: widget.cycle.prepare,
                    build: (BuildContext context, double time) => Text(
                      format(Duration(seconds: time.toInt())),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 150,
                      ),
                    ),
                    interval: Duration(milliseconds: 1000),
                    onFinished: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => FinishedPage(
                            cycle: widget.cycle,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              Container(
                child: Center(
                  child: Text(
                    "COOLDOWN",
                    style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color: Colors.orange[200]),
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(buttonIcon),
          onPressed: () {
            final isCompleted = controller.isCompleted;
            isCompleted ? print("The End") : print("Still Not Over");
            if (_isPause == false) {
              print("Pause");
              setState(() {
                _isPause = true;
                controller.pause();
                buttonIcon = Icons.play_arrow;
                backgroundColor = TinyColor(Colors.orange).darken(20);
              });
            } else {
              print("Playing");
              setState(() {
                _isPause = false;
                controller.resume();
                buttonIcon = Icons.pause;
                backgroundColor = TinyColor(Colors.orange);
              });
            }
          },
        ),
      ),
    );
  }
}
