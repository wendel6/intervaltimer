import 'package:flutter/material.dart';
import 'package:interval_timer/models/cycle_manager.dart';
import 'package:interval_timer/models/main_card_cycle_values.dart';
import 'package:interval_timer/pages/edit_page.dart';
import 'package:interval_timer/pages/timer/prepare_page.dart';
import 'package:interval_timer/widgets/main_page/lower_part_widgets/lower_info_text_column.dart';
import 'package:interval_timer/widgets/main_page/lower_part_widgets/lower_row.dart';

class LowerPart extends StatefulWidget {
  @override
  _LowerPartState createState() => _LowerPartState();
}

class _LowerPartState extends State<LowerPart> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        LowerRow(),
        if (CycleManager.cycleListSize() == 0)
          LowerInfoTextColumn()
        else
          ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.all(8),
            itemCount: CycleManager.cycleListSize(),
            itemBuilder: (BuildContext context, int index) {
              return Card(
                shadowColor: Colors.black,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0, left: 15.0),
                      child: Text(
                        CycleManager.getCycleList()[index].title,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0, left: 15.0),
                      child: Text(
                        "SETS ${CycleManager.getCycleList()[index].sets.toString()}x",
                        style: TextStyle(
                          fontSize: 17,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0, left: 15.0),
                      child: Text(
                        "WORK ${CycleManager.getCycleList()[index].work.toString()}",
                        style: TextStyle(
                          fontSize: 17,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0, left: 15.0),
                      child: Text(
                        "REST ${CycleManager.getCycleList()[index].rest.toString()}",
                        style: TextStyle(
                          fontSize: 17,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: FlatButton(
                              onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => EditPage(
                                      cycle: CycleManager.getCycleList()[index],
                                      index: index),
                                ),
                              ),
                              child: Text(
                                "EDIT",
                                style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: FlatButton(
                              onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PreparePage(
                                    cycle: CycleManager.getCycleList()[index],
                                  ),
                                ),
                              ),
                              child: Row(
                                children: [
                                  Text(
                                    "START",
                                    style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Icon(
                                    Icons.play_arrow,
                                    color: Colors.blueGrey,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
      ],
    );
  }
}
