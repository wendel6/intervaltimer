import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:interval_timer/models/cycle.dart';
import 'package:interval_timer/models/card_name.dart';
import 'package:interval_timer/models/cycle_manager.dart';
import 'package:interval_timer/models/main_card_cycle_values.dart';

class SaveButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(240.0, 00, 0, 20),
      child: FlatButton(
        onPressed: () => {
          _showMyDialog(context),
        },
        child: Row(
          children: [
            Icon(
              Icons.save,
              color: Colors.blueGrey,
              size: 30,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                "SAVE",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.blueGrey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<void> _showMyDialog(BuildContext context) async {
  final titleController = TextEditingController();

  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        contentPadding: EdgeInsets.all(10),
        title: Text('Save Preset'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              TextField(
                controller: titleController,
                cursorColor: Colors.blueGrey,
                decoration: InputDecoration(
                  labelText: "Preset Name",
                  labelStyle: TextStyle(color: Colors.amber),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.amber),
                  ),
                  border: OutlineInputBorder(),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              'CANCEL',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text(
              'SAVE',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            onPressed: () {
              CycleManager.addCycle(
                Cycle(
                  sets: MainCardCycleValues.sets,
                  rest: MainCardCycleValues.rest,
                  work: MainCardCycleValues.work,
                  title: titleController.text == "" ?
                      formatDate(
                        new DateTime.now(),
                        [
                          'Saved-',
                          yyyy,
                          '-',
                          mm,
                          '-',
                          dd,
                          ' ',
                          HH,
                          ':',
                          nn,
                          ':',
                          ss
                        ],
                      ): titleController.text,
                ),
              );

              CardName.title = null;

              // Find an alternative to refresh mains screen
              Navigator.popAndPushNamed(context, '/');
            },
          ),
        ],
      );
    },
  );
}
