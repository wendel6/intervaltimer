import 'package:flutter/material.dart';
import 'package:interval_timer/pages/timer/prepare_page.dart';

import '../../../models/card_name.dart';
import '../../../models/cycle.dart';
import '../../../models/cycle_manager.dart';
import '../../../models/main_card_cycle_values.dart';
import 'package:date_format/date_format.dart';

class StartButton extends StatefulWidget {
  final Cycle currentCycle;

  StartButton({this.currentCycle});

  @override
  _StartButtonState createState() => _StartButtonState();
}

class _StartButtonState extends State<StartButton> {
  @override
  Widget build(BuildContext context) {
    Cycle myCycle;
    //HERE
    return Expanded(
      child: FlatButton(
        onPressed: () {
          setState(() {
            myCycle = Cycle(
              sets: MainCardCycleValues.sets,
              work: MainCardCycleValues.work,
              rest: MainCardCycleValues.rest,
              prepare: 3,
            );
          });
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PreparePage(
                cycle: myCycle,
              ),
            ),
          );
        },
        color: Colors.blueGrey,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.bolt,
              color: Colors.yellow,
            ),
            Text(
              "START",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

