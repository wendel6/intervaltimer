import 'package:flutter/material.dart';

class MainCardTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      "Quickstart",
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 25,
      ),
    );
  }
}
