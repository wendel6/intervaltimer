import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../models/main_card_cycle_values.dart';
import '../../../models/secondary_card_cycle_values.dart';

class TimerCategory extends StatefulWidget {
  final String title;

  TimerCategory({Key key, this.title}) : super(key: key);

  @override
  _TimerCategoryState createState() => _TimerCategoryState();
}

class _TimerCategoryState extends State<TimerCategory> {
  String localVersionOfValue;
  format(Duration d) => d.toString().split('.')[0].padLeft(8, "0").substring(3);

  @override
  Widget build(BuildContext context) {
    if (widget.title == "Sets") {
      localVersionOfValue = MainCardCycleValues.sets.toString();
    } else if (widget.title == "Work") {
      localVersionOfValue = format(Duration(seconds: MainCardCycleValues.work));
    } else if (widget.title == "Rest") {
      localVersionOfValue = format(Duration(seconds: MainCardCycleValues.rest));
    } else if (widget.title == "Prepare") {
      localVersionOfValue = format(Duration(seconds: SecCycleValues.prepare));
    } else if (widget.title == "Cooldown") {
      localVersionOfValue = format(Duration(seconds: SecCycleValues.cooldown));
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Text(
            widget.title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FlatButton(
                  onPressed: () => setState(() {
                    print(MainCardCycleValues.sets);
                    print(MainCardCycleValues.work);
                    print(MainCardCycleValues.rest);
                    if (widget.title == "Sets") {
                      if (MainCardCycleValues.sets - 1 > 0) {
                        MainCardCycleValues.sets -= 1;
                        localVersionOfValue = MainCardCycleValues.sets.toString();
                      }
                    } else if (widget.title == "Work") {
                      if (MainCardCycleValues.work - 1 > 0) {
                        MainCardCycleValues.work -= 1;
                        localVersionOfValue = format(Duration(seconds: MainCardCycleValues.work));
                      }
                    } else if (widget.title == "Rest") {
                      if (MainCardCycleValues.rest - 1 >= 0) {
                        MainCardCycleValues.rest -= 1;
                        localVersionOfValue = format(Duration(seconds: MainCardCycleValues.rest));
                      }
                    } else if (widget.title == "Prepare") {
                      if (SecCycleValues.prepare - 1 >= 0) {
                        SecCycleValues.prepare -= 1;
                        localVersionOfValue = format(Duration(seconds: SecCycleValues.prepare));
                      }
                    } else if (widget.title == "Cooldown") {
                      if (SecCycleValues.cooldown - 1 >= 0) {
                        SecCycleValues.cooldown -= 1;
                        localVersionOfValue = format(Duration(seconds: SecCycleValues.cooldown));
                      }
                    }
                  }),

                  child: Container(
                    color: Colors.black,
                    child: Icon(
                      Icons.remove,
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(width: 30,),
                Text(
                  localVersionOfValue,
                  // widget.value.toString(),
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(width: 30,),
                FlatButton(
                  onPressed: () => setState(() {
                    if (widget.title == "Sets") {
                      MainCardCycleValues.sets += 1;
                      localVersionOfValue = MainCardCycleValues.sets.toString();
                    } else if (widget.title == "Work") {
                      MainCardCycleValues.work += 1;
                      localVersionOfValue = format(Duration(seconds: MainCardCycleValues.work));
                    } else if (widget.title == "Rest") {
                      MainCardCycleValues.rest += 1;
                      localVersionOfValue = format(Duration(seconds: MainCardCycleValues.rest));
                    } else if (widget.title == "Prepare") {
                      SecCycleValues.prepare += 1;
                      localVersionOfValue = format(Duration(seconds: SecCycleValues.prepare));
                    } else if (widget.title == "Cooldown") {
                      SecCycleValues.cooldown += 1;
                      localVersionOfValue = format(Duration(seconds: SecCycleValues.cooldown));
                    }
                  }),
                  child: Container(
                    color: Colors.black,
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
