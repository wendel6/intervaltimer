import 'package:flutter/material.dart';

import '../../models/cycle.dart';
import '../../models/main_card_cycle_values.dart';
import 'main_card_widgets/main_card_title.dart';
import 'main_card_widgets/save_button.dart';
import 'main_card_widgets/start_button.dart';
import 'main_card_widgets/timer_category.dart';

class MainCard extends StatefulWidget {
  @override
  _MainCardState createState() => _MainCardState();
}

class _MainCardState extends State<MainCard> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10, left: 10, right: 10),
      child: Card(
        child: ExpansionTile(
          onExpansionChanged: (bool expanding) =>
              setState(() => isExpanded = !isExpanded),
          leading: Text(
            "Quickstart",
            style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          trailing: Icon(isExpanded
              ? Icons.arrow_drop_up_sharp
              : Icons.arrow_drop_down_sharp, color: Colors.black,),
          initiallyExpanded: true,
          children: <Widget>[
            Container(
              height: 400,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  TimerCategory(title: "Sets"),
                  TimerCategory(title: "Work"),
                  TimerCategory(title: "Rest"),
                  SizedBox(
                    height: 15,
                  ),
                  SaveButton(),
                  StartButton(
                    currentCycle: Cycle(
                      sets: MainCardCycleValues.sets,
                      work: MainCardCycleValues.work,
                      rest: MainCardCycleValues.rest,
                      prepare: 3,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
