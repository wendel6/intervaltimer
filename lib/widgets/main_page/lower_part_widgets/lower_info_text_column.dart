import 'package:flutter/material.dart';

class LowerInfoTextColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 70.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("To create your first preset", style: TextStyle(),),
            Text("use the SAVE button to quick save."),
            Text("For advanced settings use the ADD button."),
            SizedBox(
              height: 70,
            )
          ],
        ),
      ),
    );
  }
}
