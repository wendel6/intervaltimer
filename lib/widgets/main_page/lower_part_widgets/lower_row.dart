import 'package:flutter/material.dart';
import '../../../models/cycle.dart';
import '../../../models/cycle_manager.dart';
import '../../../pages/edit_page.dart';

class LowerRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            (CycleManager.cycleListSize() == 0)
                ? "NO PRESETS YET"
                : "YOUR PRESETS",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
          ),
          FlatButton(
            height: 30,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100.0),
              side: BorderSide(
                width: 2,
                color: Colors.blueGrey,
              ),
            ),
            color: Colors.grey[300],
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => EditPage(
                    cycle: Cycle(
                      sets: 1,
                      work: 1,
                      rest: 1,
                    ),
                ),
              ),
            ),
            child: Container(
              child: Text(
                "+ ADD",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blueGrey,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
