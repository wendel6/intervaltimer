import 'dart:async';

import 'package:flutter/material.dart';
import 'package:volume/volume.dart';


class NavBar extends StatefulWidget {
  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  @override
  void initState() {
    bool _initVal = true;
    initVolumeState();
    super.initState();
  }

  Future<void> initVolumeState() async {
    await Volume.controlVolume(AudioManager.STREAM_RING);
  }

  double _val = 0.1;
  bool _initVal = true;
  Timer timer;
  Icon navBar = Icon(Icons.volume_up);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
            icon: Icon(Icons.volume_up, color: Colors.white),
            onPressed: () => print("Hey")),
        Container(
          width: 300,
          child: Slider(
            activeColor: Colors.white,
            value: _initVal ? 7.5 : _val,
            min: 0.0,
            max: 15.0,
            divisions: 100,
            onChanged: (val) {
              _initVal = false;
              _val = val;
              setState(() {});
              if (timer != null) {
                timer.cancel();
              }

              print(val);
              //use timer for the smoother sliding
              timer = Timer(Duration(milliseconds: 200), () {
                Volume.setVol(val.toInt());
              });
            },
          ),
        )
      ],
    );
  }
}
