import 'package:flutter/material.dart';
import 'package:interval_timer/models/main_card_cycle_values.dart';
import 'package:timer_count_down/timer_controller.dart';

class CycleExitButton extends StatelessWidget {
  CountdownController countdownController;

  CycleExitButton(countdownController);

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: 55,
        width: 150,
        child: TextButton(
          style: ButtonStyle(
            elevation:
            MaterialStateProperty.all<double>(8),
            shadowColor:
            MaterialStateProperty.all<Color>(Colors.black),
            backgroundColor:
            MaterialStateProperty.all<Color>(Colors.blueGrey),
            shape:
            MaterialStateProperty.all<OutlinedBorder>(
              RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(100.0),
              ),
            ),
          ),
          onLongPress: () {
            Navigator.pushNamed(context, '/');
          },
          child: Container(
            child: Text(
              "HOLD TO EXIT",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}