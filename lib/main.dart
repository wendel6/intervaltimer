import 'package:flutter/material.dart';

import 'pages/edit_page.dart';
import 'pages/main_page.dart';
import 'pages/timer/prepare_page.dart';
import 'pages/timer/work_page.dart';
import 'pages/timer/rest_page.dart';
import 'pages/timer/finished_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => MainPage(),
        '/edit': (context) => EditPage(),
        '/timer': (context) => PreparePage(),
        '/work': (context) => WorkPage(),
        '/rest': (context) => RestPage(),
        '/finished': (context) => FinishedPage(),
      },
    );
  }
}

